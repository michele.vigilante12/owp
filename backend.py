import yaml
import requests
from xml.dom import minidom
import datetime


def get_meteo(city):
    url = api_key.format(city)
    xmldata = requests.get(url)
    xdp = minidom.parseString(xmldata.text)
    return Meteo(xdp)


with open('config.yml', 'r') as yaml_f:
    config = yaml.load(yaml_f, Loader=yaml.FullLoader)

api_key = config["key"]


class Meteo:
    def __init__(self, xdp):
        self.city = xdp.getElementsByTagName("city")[0].getAttribute("name")
        self.country = xdp.getElementsByTagName("country")[0].firstChild.nodeValue
        self.lon = xdp.getElementsByTagName("coord")[0].getAttribute("lon")
        self.lat = xdp.getElementsByTagName("coord")[0].getAttribute("lat")
        self.temp_corr = xdp.getElementsByTagName("temperature")[0].getAttribute("value")
        self.humidity = xdp.getElementsByTagName("humidity")[0].getAttribute("value")
        self.temp_min = xdp.getElementsByTagName("temperature")[0].getAttribute("min")
        self.temp_max = xdp.getElementsByTagName("temperature")[0].getAttribute("max")
        self.icon = xdp.getElementsByTagName("weather")[0].getAttribute("icon")
        timezone = xdp.getElementsByTagName("timezone")[0].firstChild.nodeValue
        self.weather = xdp.getElementsByTagName("weather")[0].getAttribute("value")
        utc_now = datetime.datetime.utcnow()
        self.current_time = utc_now - datetime.timedelta(microseconds=utc_now.microsecond) + datetime.timedelta(seconds=int(timezone))
