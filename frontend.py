import backend
from tkinter import *
from tkinter import messagebox
from PIL import Image, ImageTk


class Window:
    def __init__(self, win):
        self.win = win
        self.humidity_lbl = Label(fg="#c8a2c8", font="TimesNewRoman 12 bold")
        self.time_lbl = Label(fg="black", font="Calibri, 10")
        self.weather_lbl = Label(fg="blue", font="Helvetica 12 bold")
        self.temp_lbl = Label(fg="red", font="Calibri 10 bold")
        self.image = Label()
        self.location_lbl = Label(fg="black", font="Calibri 12 bold")
        self.coord_lbl = Label(fg="black", font="Calibri 12 bold")
        self.lbl = Label(win, text='Inserisci città: ', font="Calibri 10 bold")
        self.txtfld = Entry(bg="lightblue")
        self.btn = Button(win, text='Cerca', bg="lightgreen", command=self.search)

        self.lbl.place(x=20, y=16)
        self.txtfld.place(x=100, y=20)
        self.btn.place(x=243, y=16)

    def search(self):
        city = str(self.txtfld.get())
        try:
            meteo = backend.get_meteo(city)
        except IndexError:
            messagebox.showerror('Errore', f'{city} non esiste.')
            return

        self.location_lbl['text'] = f'{meteo.city},{meteo.country}'
        self.location_lbl.place(x=10, y=60)

        self.coord_lbl['text'] = f'{meteo.lon},{meteo.lat}'
        self.coord_lbl.place(x=170, y=60)

        self.weather_lbl['text'] = meteo.weather
        self.weather_lbl.place(x=110, y=90)

        img = Image.open('Resources/images/{}.png'.format(meteo.icon))
        self.image = ImageTk.PhotoImage(img)
        Label(self.win, image=self.image).place(relx=0.3, rely=0.3, relwidth=0.33, relheight=0.25)

        self.temp_lbl['text'] = f'Temperature: min {meteo.temp_min} °C, corrente {meteo.temp_corr} °C, max {meteo.temp_max} °C'
        self.temp_lbl.place(x=15, y=230)

        self.humidity_lbl['text'] = f'Umidità{meteo.humidity}%'
        self.humidity_lbl.place(x=15, y=270)

        self.time_lbl['text'] = f'Ora locale: {meteo.current_time}'
        self.time_lbl.place(x=15, y=320)


window = Tk()
mywin = Window(window)
window.title('Meteo')
window.geometry("335x400+350+200")
window.mainloop()
